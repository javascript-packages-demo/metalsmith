# [metalsmith](https://www.npmjs.com/package/metalsmith)

An extremely simple, pluggable static site generator. https://metalsmith.io

* [*How to Create a Static Site with Metalsmith*](https://www.sitepoint.com/create-static-site-metalsmith/) 2016
* https://staticgen.com/